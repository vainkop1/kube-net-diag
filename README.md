# Kube-net-diag

Kube-net-diag: tools to check your Kubernetes cluster network!

Usage:
```
./kube-net-diag.sh <Server Node>

Set your custom parameters in steps/run.sh
```

Example:
```
$ ./kube-net-diag.sh kn4-example.com
[Setup]
####
#### kube-net-diag.sh <Server Node>
####
####
#### Checking possible previous installations
####
#### Previous installation detected!
NAME                                        READY   STATUS    RESTARTS   AGE     IP          NODE           NOMINATED NODE   READINESS GATES
kube-net-diag-deployment-5cd9976d74-g6c4x   1/1     Running   0          3m26s   10.34.0.5   kn2-example.com   <none>           <none>
####
#### Deleting previous installation
deployment.apps "kube-net-diag-deployment" deleted
service "kube-net-diag" deleted
daemonset.apps "kube-net-diag-clients" deleted
####
####
#### Starting kube-net-diag
####
deployment.apps/kube-net-diag-deployment created
service/kube-net-diag created
daemonset.apps/kube-net-diag-clients created
####
#### Waiting for kube-net-diag to start on node kn4-example.com
####
#### kube-net-diag is running on node kn4-example.com !
####
[Run]
####
#### Mtr on kn3-example.com connecting to pod on target node kn4-example.com
Start: 2020-08-05T21:10:40+0000
HOST: kube-net-diag-clients-64xpj Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 10.43.0.0             0.0%     5    0.1   0.2   0.1   0.3   0.1
  2.|-- 105.201.105.65        0.0%     5    0.8   0.5   0.3   0.8   0.3
  3.|-- 135.132.229.145      40.0%     5    0.5  11.7   0.5  33.8  19.1
  4.|-- 135.132.102.29        0.0%     5    2.8   3.0   2.8   3.1   0.1
  5.|-- 135.132.102.154       0.0%     5    3.3  10.5   3.1  39.8  16.4
  6.|-- ???                  100.0     5    0.0   0.0   0.0   0.0   0.0
Start: 2020-08-05T21:10:51+0000
HOST: kube-net-diag-clients-64xpj Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 10.43.0.0             0.0%     5    0.2   0.2   0.1   0.2   0.0
  2.|-- 105.201.105.65        0.0%     5    0.6   0.4   0.3   0.6   0.1
  3.|-- 135.132.229.145      40.0%     5  7268. 4830.  13.0 7268. 4171.9
  4.|-- 135.132.102.206       0.0%     5    0.7   0.6   0.5   0.8   0.1
  5.|-- 109.12.123.179         0.0%     5    0.5   0.4   0.4   0.5   0.1
####
####
Client on kn3-example.com:  Connecting to host kube-net-diag, port 5201
Client on kn3-example.com:  [  5] local 10.43.0.9 port 33082 connected to 10.104.16.247 port 5201
Client on kn3-example.com:  [ ID] Interval           Transfer     Bitrate         Retr  Cwnd
Client on kn3-example.com:  [  5]   0.00-1.00   sec   108 MBytes   909 Mbits/sec   91    816 KBytes       
Client on kn3-example.com:  [  5]   1.00-2.00   sec   107 MBytes   897 Mbits/sec   20    671 KBytes       
Client on kn3-example.com:  [  5]   2.00-3.00   sec   108 MBytes   904 Mbits/sec    0    777 KBytes       
Client on kn3-example.com:  [  5]   3.00-4.00   sec   107 MBytes   900 Mbits/sec   18    625 KBytes       
Client on kn3-example.com:  [  5]   4.00-5.00   sec   107 MBytes   901 Mbits/sec    0    737 KBytes       
Client on kn3-example.com:  [  5]   5.00-6.00   sec   107 MBytes   898 Mbits/sec   109    592 KBytes       
Client on kn3-example.com:  [  5]   6.00-7.00   sec   108 MBytes   906 Mbits/sec    0    709 KBytes       
Client on kn3-example.com:  [  5]   7.00-8.00   sec   107 MBytes   900 Mbits/sec   17    590 KBytes       
Client on kn3-example.com:  [  5]   8.00-9.00   sec   107 MBytes   900 Mbits/sec   18    544 KBytes       
Client on kn3-example.com:  [  5]   9.00-10.00  sec   108 MBytes   902 Mbits/sec    0    671 KBytes       
Client on kn3-example.com:  - - - - - - - - - - - - - - - - - - - - - - - - -
Client on kn3-example.com:  [ ID] Interval           Transfer     Bitrate         Retr
Client on kn3-example.com:  [  5]   0.00-10.00  sec  1.05 GBytes   902 Mbits/sec  135             sender
Client on kn3-example.com:  [  5]   0.00-10.00  sec  1.05 GBytes   900 Mbits/sec                  receiver
Client on kn3-example.com:  
Client on kn3-example.com:  iperf Done.
####
####
#### Mtr on km2-example.com connecting to pod on target node kn4-example.com
Start: 2020-08-05T21:11:13+0000
HOST: kube-net-diag-clients-6gz29 Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 10.32.0.1             0.0%     5    0.2   0.2   0.2   0.3   0.0
  2.|-- 172.31.1.1            0.0%     5    0.5   0.4   0.3   0.6   0.1
  3.|-- 127.69.97.79          0.0%     5    0.4   0.4   0.4   0.5   0.0
  4.|-- ???                  100.0     5    0.0   0.0   0.0   0.0   0.0
  5.|-- 135.132.231.65        0.0%     5    1.2   3.2   1.1  11.2   4.5
  6.|-- 135.132.132.133       0.0%     5  1006. 406.5   0.6 1023. 555.7
  7.|-- 135.132.203.125       0.0%     5    3.3   3.2   3.0   3.3   0.1
  8.|-- 135.132.252.89        0.0%     5   11.9  10.6   3.4  12.8   4.1
  9.|-- ???                  100.0     5    0.0   0.0   0.0   0.0   0.0
Start: 2020-08-05T21:11:24+0000
HOST: kube-net-diag-clients-6gz29 Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 10.32.0.1             0.0%     5    0.2   0.2   0.2   0.3   0.1
  2.|-- 172.31.1.1            0.0%     5    0.5   0.6   0.4   0.9   0.2
  3.|-- 127.69.97.79          0.0%     5    0.6   0.6   0.4   0.8   0.1
  4.|-- ???                  100.0     5    0.0   0.0   0.0   0.0   0.0
  5.|-- 135.132.231.69        0.0%     5    1.3   1.3   1.1   1.6   0.2
  6.|-- 135.132.132.129      60.0%     5  7288. 3645.   3.2 7288. 5151.4
  7.|-- 135.132.102.210       0.0%     5    1.1   1.0   0.7   1.2   0.2
  8.|-- 109.12.123.179         0.0%     5    0.7   0.7   0.7   0.8   0.0
####
####
Client on km2-example.com:  Connecting to host kube-net-diag, port 5201
Client on km2-example.com:  [  5] local 10.32.0.2 port 59284 connected to 10.104.16.247 port 5201
Client on km2-example.com:  [ ID] Interval           Transfer     Bitrate         Retr  Cwnd
Client on km2-example.com:  [  5]   0.00-1.00   sec   104 MBytes   872 Mbits/sec  171    405 KBytes       
Client on km2-example.com:  [  5]   1.00-2.00   sec   105 MBytes   883 Mbits/sec   11    427 KBytes       
Client on km2-example.com:  [  5]   2.00-3.00   sec   106 MBytes   888 Mbits/sec    1    432 KBytes       
Client on km2-example.com:  [  5]   3.00-4.00   sec   106 MBytes   888 Mbits/sec   19    462 KBytes       
Client on km2-example.com:  [  5]   4.00-5.00   sec   107 MBytes   896 Mbits/sec   36    434 KBytes       
Client on km2-example.com:  [  5]   5.00-6.00   sec   108 MBytes   904 Mbits/sec   19    418 KBytes       
Client on km2-example.com:  [  5]   6.00-7.00   sec   105 MBytes   878 Mbits/sec    0    570 KBytes       
Client on km2-example.com:  [  5]   7.00-8.00   sec   107 MBytes   900 Mbits/sec    9    403 KBytes       
Client on km2-example.com:  [  5]   8.00-9.00   sec   105 MBytes   877 Mbits/sec   17    414 KBytes       
Client on km2-example.com:  [  5]   9.00-10.00  sec   107 MBytes   900 Mbits/sec   11    436 KBytes       
Client on km2-example.com:  - - - - - - - - - - - - - - - - - - - - - - - - -
Client on km2-example.com:  [ ID] Interval           Transfer     Bitrate         Retr
Client on km2-example.com:  [  5]   0.00-10.00  sec  1.03 GBytes   889 Mbits/sec  294             sender
Client on km2-example.com:  [  5]   0.00-10.04  sec  1.03 GBytes   884 Mbits/sec                  receiver
Client on km2-example.com:  
Client on km2-example.com:  iperf Done.
####
####
#### Mtr on km3-example.com connecting to pod on target node kn4-example.com
Start: 2020-08-05T21:11:46+0000
HOST: kube-net-diag-clients-k6xjw Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 10.44.0.0             0.0%     5    0.2   0.2   0.2   0.3   0.0
  2.|-- 172.31.1.1            0.0%     5    0.4   0.4   0.2   0.6   0.1
  3.|-- 127.69.97.94          0.0%     5    0.3   0.4   0.3   0.6   0.1
  4.|-- ???                  100.0     5    0.0   0.0   0.0   0.0   0.0
  5.|-- 135.132.231.65        0.0%     5    1.3   1.1   1.0   1.3   0.1
  6.|-- 135.132.132.133      20.0%     5  7153. 2043.   0.7 7153. 3440.3
  7.|-- 135.132.102.225       0.0%     5    3.1   3.0   2.9   3.2   0.1
  8.|-- 135.132.102.154       0.0%     5   11.4   8.2   3.2  15.5   5.3
  9.|-- ???                  100.0     5    0.0   0.0   0.0   0.0   0.0
Start: 2020-08-05T21:11:57+0000
HOST: kube-net-diag-clients-k6xjw Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 10.44.0.0             0.0%     5    0.3   0.2   0.1   0.3   0.1
  2.|-- 172.31.1.1            0.0%     5    0.4   0.4   0.3   0.5   0.1
  3.|-- 127.69.97.94          0.0%     5    0.3   0.4   0.3   0.4   0.0
  4.|-- ???                  100.0     5    0.0   0.0   0.0   0.0   0.0
  5.|-- 135.132.231.65        0.0%     5    1.0   1.1   0.9   1.2   0.1
  6.|-- 135.132.132.133      20.0%     5  7284. 3632.   0.8 7284. 4185.4
  7.|-- 135.132.102.206       0.0%     5    0.7   0.8   0.7   1.0   0.1
  8.|-- 109.12.123.179         0.0%     5    1.5   0.8   0.5   1.5   0.4
####
####
Client on km3-example.com:  Connecting to host kube-net-diag, port 5201
Client on km3-example.com:  [  5] local 10.44.0.2 port 58956 connected to 10.104.16.247 port 5201
Client on km3-example.com:  [ ID] Interval           Transfer     Bitrate         Retr  Cwnd
Client on km3-example.com:  [  5]   0.00-1.00   sec   102 MBytes   853 Mbits/sec  228    387 KBytes       
Client on km3-example.com:  [  5]   1.00-2.00   sec   105 MBytes   878 Mbits/sec   25    380 KBytes       
Client on km3-example.com:  [  5]   2.00-3.00   sec   100 MBytes   840 Mbits/sec    1    533 KBytes       
Client on km3-example.com:  [  5]   3.00-4.00   sec   105 MBytes   882 Mbits/sec   11    1098 KBytes       
Client on km3-example.com:  [  5]   4.00-5.00   sec   103 MBytes   867 Mbits/sec   13    507 KBytes       
Client on km3-example.com:  [  5]   5.00-6.00   sec   101 MBytes   843 Mbits/sec   42    476 KBytes       
Client on km3-example.com:  [  5]   6.00-7.00   sec   103 MBytes   862 Mbits/sec    2    1096 KBytes       
Client on km3-example.com:  [  5]   7.00-8.00   sec   105 MBytes   880 Mbits/sec   23    438 KBytes       
Client on km3-example.com:  [  5]   8.00-9.00   sec   106 MBytes   886 Mbits/sec    2    407 KBytes       
Client on km3-example.com:  [  5]   9.00-10.00  sec   101 MBytes   844 Mbits/sec  107    547 KBytes       
Client on km3-example.com:  - - - - - - - - - - - - - - - - - - - - - - - - -
Client on km3-example.com:  [ ID] Interval           Transfer     Bitrate         Retr
Client on km3-example.com:  [  5]   0.00-10.00  sec  1.01 GBytes   864 Mbits/sec  454             sender
Client on km3-example.com:  [  5]   0.00-10.04  sec  1.00 GBytes   859 Mbits/sec                  receiver
Client on km3-example.com:  
Client on km3-example.com:  iperf Done.
####
####
#### Mtr on km1-example.com connecting to pod on target node kn4-example.com
Start: 2020-08-05T21:12:20+0000
HOST: kube-net-diag-clients-nbthv Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 10.40.0.0             0.0%     5    0.2   0.2   0.1   0.2   0.0
  2.|-- 172.31.1.1            0.0%     5    0.4   0.5   0.4   0.6   0.1
  3.|-- 127.69.97.59          0.0%     5    0.7   0.5   0.3   0.7   0.1
  4.|-- ???                  100.0     5    0.0   0.0   0.0   0.0   0.0
  5.|-- 135.132.231.69        0.0%     5    2.8   5.8   2.8  12.0   3.8
  6.|-- 135.132.132.125       0.0%     5    0.8   2.9   0.8  10.9   4.5
  7.|-- 135.132.102.225       0.0%     5    3.8   3.5   3.2   3.8   0.2
  8.|-- 135.132.102.154       0.0%     5   16.8   6.1   3.3  16.8   6.0
  9.|-- ???                  100.0     5    0.0   0.0   0.0   0.0   0.0
Start: 2020-08-05T21:12:31+0000
HOST: kube-net-diag-clients-nbthv Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 10.40.0.0             0.0%     5    0.2   0.3   0.2   0.5   0.1
  2.|-- 172.31.1.1            0.0%     5    0.5   0.4   0.4   0.5   0.1
  3.|-- 127.69.97.59          0.0%     5    0.5   0.5   0.5   0.5   0.0
  4.|-- ???                  100.0     5    0.0   0.0   0.0   0.0   0.0
  5.|-- 135.132.231.69        0.0%     5    1.5   2.1   1.2   4.8   1.6
  6.|-- 135.132.132.133       0.0%     5    1.0   2.5   0.8   6.3   2.4
  7.|-- 135.132.102.210       0.0%     5    1.0   0.9   0.7   1.1   0.2
  8.|-- 109.12.123.179         0.0%     5    0.7   0.6   0.4   0.8   0.1
####
####
Client on km1-example.com:  Connecting to host kube-net-diag, port 5201
Client on km1-example.com:  [  5] local 10.40.0.1 port 52738 connected to 10.104.16.247 port 5201
Client on km1-example.com:  [ ID] Interval           Transfer     Bitrate         Retr  Cwnd
Client on km1-example.com:  [  5]   0.00-1.00   sec   100 MBytes   840 Mbits/sec   99    354 KBytes       
Client on km1-example.com:  [  5]   1.00-2.00   sec   106 MBytes   888 Mbits/sec   15    365 KBytes       
Client on km1-example.com:  [  5]   2.00-3.00   sec   102 MBytes   859 Mbits/sec   33    518 KBytes       
Client on km1-example.com:  [  5]   3.00-4.00   sec   108 MBytes   904 Mbits/sec   26    502 KBytes       
Client on km1-example.com:  [  5]   4.00-5.00   sec   104 MBytes   873 Mbits/sec   55    455 KBytes       
Client on km1-example.com:  [  5]   5.00-6.00   sec   102 MBytes   857 Mbits/sec   11    465 KBytes       
Client on km1-example.com:  [  5]   6.00-7.00   sec   105 MBytes   881 Mbits/sec    0    603 KBytes       
Client on km1-example.com:  [  5]   7.00-8.00   sec   104 MBytes   868 Mbits/sec   32    411 KBytes       
Client on km1-example.com:  [  5]   8.00-9.00   sec  94.4 MBytes   792 Mbits/sec    9    437 KBytes       
Client on km1-example.com:  [  5]   9.00-10.00  sec   105 MBytes   880 Mbits/sec   12    438 KBytes       
Client on km1-example.com:  - - - - - - - - - - - - - - - - - - - - - - - - -
Client on km1-example.com:  [ ID] Interval           Transfer     Bitrate         Retr
Client on km1-example.com:  [  5]   0.00-10.00  sec  1.01 GBytes   864 Mbits/sec  292             sender
Client on km1-example.com:  [  5]   0.00-10.04  sec  1.00 GBytes   860 Mbits/sec                  receiver
Client on km1-example.com:  
Client on km1-example.com:  iperf Done.
####
####
#### Mtr on kn1-example.com connecting to pod on target node kn4-example.com
Start: 2020-08-05T21:12:54+0000
HOST: kube-net-diag-clients-qtv4g Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 10.38.0.0             0.0%     5    0.1   0.1   0.1   0.1   0.0
  2.|-- 144.119.6.65          0.0%     5    1.3   0.6   0.4   1.3   0.4
  3.|-- 135.132.102.205       0.0%     5  1020. 204.4   0.4 1020. 455.9
  4.|-- 135.132.102.29        0.0%     5    3.0   2.9   2.8   3.1   0.1
  5.|-- 135.132.252.89        0.0%     5    9.8   8.4   3.1  15.1   4.7
  6.|-- ???                  100.0     5    0.0   0.0   0.0   0.0   0.0
Start: 2020-08-05T21:13:05+0000
HOST: kube-net-diag-clients-qtv4g Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 10.38.0.0             0.0%     5    0.1   0.1   0.1   0.1   0.0
  2.|-- 144.119.6.65          0.0%     5    0.4   0.5   0.4   0.5   0.1
  3.|-- 109.12.123.179         0.0%     5    0.4   0.3   0.2   0.4   0.1
####
####
Client on kn1-example.com:  Connecting to host kube-net-diag, port 5201
Client on kn1-example.com:  [  5] local 10.38.0.8 port 46012 connected to 10.104.16.247 port 5201
Client on kn1-example.com:  [ ID] Interval           Transfer     Bitrate         Retr  Cwnd
Client on kn1-example.com:  [  5]   0.00-1.00   sec   108 MBytes   908 Mbits/sec   58    756 KBytes       
Client on kn1-example.com:  [  5]   1.00-2.00   sec   107 MBytes   896 Mbits/sec   30    487 KBytes       
Client on kn1-example.com:  [  5]   2.00-3.00   sec   108 MBytes   904 Mbits/sec    0    626 KBytes       
Client on kn1-example.com:  [  5]   3.00-4.00   sec   108 MBytes   903 Mbits/sec    0    736 KBytes       
Client on kn1-example.com:  [  5]   4.00-5.00   sec   108 MBytes   908 Mbits/sec    0    834 KBytes       
Client on kn1-example.com:  [  5]   5.00-6.00   sec   108 MBytes   902 Mbits/sec    0    921 KBytes       
Client on kn1-example.com:  [  5]   6.00-7.00   sec   108 MBytes   903 Mbits/sec    0    999 KBytes       
Client on kn1-example.com:  [  5]   7.00-8.00   sec   108 MBytes   903 Mbits/sec    0   1.00 MBytes       
Client on kn1-example.com:  [  5]   8.00-9.00   sec   108 MBytes   902 Mbits/sec    0   1.00 MBytes       
Client on kn1-example.com:  [  5]   9.00-10.00  sec   107 MBytes   897 Mbits/sec    0   1.00 MBytes       
Client on kn1-example.com:  - - - - - - - - - - - - - - - - - - - - - - - - -
Client on kn1-example.com:  [ ID] Interval           Transfer     Bitrate         Retr
Client on kn1-example.com:  [  5]   0.00-10.00  sec  1.05 GBytes   902 Mbits/sec   88             sender
Client on kn1-example.com:  [  5]   0.00-10.04  sec  1.05 GBytes   898 Mbits/sec                  receiver
Client on kn1-example.com:  
Client on kn1-example.com:  iperf Done.
####
####
#### Mtr on kn2-example.com connecting to pod on target node kn4-example.com
Start: 2020-08-05T21:13:26+0000
HOST: kube-net-diag-clients-t5db6 Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 10.34.0.0             0.0%     5    0.0   0.1   0.0   0.1   0.0
  2.|-- 46.4.45.225           0.0%     5    0.6   5.0   0.5  15.2   6.6
  3.|-- 135.132.102.121       0.0%     5  1024. 208.1   0.4 1024. 456.5
  4.|-- 135.132.102.225       0.0%     5    3.0   2.9   2.9   3.0   0.1
  5.|-- 135.132.102.154       0.0%     5    3.2   3.1   3.1   3.2   0.0
  6.|-- ???                  100.0     5    0.0   0.0   0.0   0.0   0.0
Start: 2020-08-05T21:13:37+0000
HOST: kube-net-diag-clients-t5db6 Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 10.34.0.0             0.0%     5    0.1   0.1   0.1   0.1   0.0
  2.|-- 46.4.45.225           0.0%     5    0.7   0.6   0.4   0.8   0.2
  3.|-- 135.132.102.81       20.0%     5    8.3   8.9   4.2  18.2   6.5
  4.|-- 135.132.102.206       0.0%     5    0.6   0.6   0.5   0.7   0.1
  5.|-- 109.12.123.179         0.0%     5    0.4   0.4   0.4   0.5   0.0
####
####
Client on kn2-example.com:  Connecting to host kube-net-diag, port 5201
Client on kn2-example.com:  [  5] local 10.34.0.5 port 51014 connected to 10.104.16.247 port 5201
Client on kn2-example.com:  [ ID] Interval           Transfer     Bitrate         Retr  Cwnd
Client on kn2-example.com:  [  5]   0.00-1.00   sec  3.71 GBytes  31.9 Gbits/sec    0    588 KBytes       
Client on kn2-example.com:  [  5]   1.00-2.00   sec  4.44 GBytes  38.1 Gbits/sec    0    648 KBytes       
Client on kn2-example.com:  [  5]   2.00-3.00   sec  4.32 GBytes  37.1 Gbits/sec    0    750 KBytes       
Client on kn2-example.com:  [  5]   3.00-4.00   sec  4.55 GBytes  39.1 Gbits/sec    0    869 KBytes       
Client on kn2-example.com:  [  5]   4.00-5.00   sec  4.109 GBytes  38.6 Gbits/sec    0    869 KBytes       
Client on kn2-example.com:  [  5]   5.00-6.00   sec  4.32 GBytes  37.1 Gbits/sec    0    869 KBytes       
Client on kn2-example.com:  [  5]   6.00-7.00   sec  4.37 GBytes  37.5 Gbits/sec    0    869 KBytes       
Client on kn2-example.com:  [  5]   7.00-8.00   sec  4.30 GBytes  36.9 Gbits/sec    0    912 KBytes       
Client on kn2-example.com:  [  5]   8.00-9.00   sec  4.53 GBytes  39.0 Gbits/sec    0   1005 KBytes       
Client on kn2-example.com:  [  5]   9.00-10.00  sec  4.36 GBytes  37.5 Gbits/sec    0   1.03 MBytes       
Client on kn2-example.com:  - - - - - - - - - - - - - - - - - - - - - - - - -
Client on kn2-example.com:  [ ID] Interval           Transfer     Bitrate         Retr
Client on kn2-example.com:  [  5]   0.00-10.00  sec  43.4 GBytes  37.3 Gbits/sec    0             sender
Client on kn2-example.com:  [  5]   0.00-10.04  sec  43.4 GBytes  37.1 Gbits/sec                  receiver
Client on kn2-example.com:  
Client on kn2-example.com:  iperf Done.
####
####
#### Mtr on kn4-example.com connecting to pod on target node kn4-example.com
Start: 2020-08-05T21:13:59+0000
HOST: kube-net-diag-clients-zp4s6 Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 10.35.128.0           0.0%     5    0.1   0.1   0.1   0.2   0.0
  2.|-- 109.12.123.129         0.0%     5    1.0   0.6   0.4   1.0   0.2
  3.|-- 135.132.102.205       0.0%     5   13.7 218.4   2.9 1030. 454.4
  4.|-- 135.132.102.225       0.0%     5    2.8   6.0   2.8  18.2   6.8
  5.|-- 135.132.252.89        0.0%     5    3.2   4.6   3.1  10.4   3.2
  6.|-- ???                  100.0     5    0.0   0.0   0.0   0.0   0.0
Start: 2020-08-05T21:14:10+0000
HOST: kube-net-diag-clients-zp4s6 Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 109.12.123.179         0.0%     5    0.1   0.1   0.1   0.1   0.0
####
####
Client on kn4-example.com:  Connecting to host kube-net-diag, port 5201
Client on kn4-example.com:  [  5] local 10.35.128.3 port 60438 connected to 10.104.16.247 port 5201
Client on kn4-example.com:  [ ID] Interval           Transfer     Bitrate         Retr  Cwnd
Client on kn4-example.com:  [  5]   0.00-1.00   sec   109 MBytes   913 Mbits/sec    0   1.05 MBytes       
Client on kn4-example.com:  [  5]   1.00-2.00   sec   106 MBytes   888 Mbits/sec   57    423 KBytes       
Client on kn4-example.com:  [  5]   2.00-3.00   sec   106 MBytes   891 Mbits/sec    0    571 KBytes       
Client on kn4-example.com:  [  5]   3.00-4.00   sec   107 MBytes   898 Mbits/sec    0    690 KBytes       
Client on kn4-example.com:  [  5]   4.00-5.00   sec   108 MBytes   904 Mbits/sec   109    577 KBytes       
Client on kn4-example.com:  [  5]   5.00-6.00   sec   102 MBytes   854 Mbits/sec   32    372 KBytes       
Client on kn4-example.com:  [  5]   6.00-7.00   sec  98.9 MBytes   829 Mbits/sec    0    529 KBytes       
Client on kn4-example.com:  [  5]   7.00-8.00   sec  98.1 MBytes   823 Mbits/sec   10    1099 KBytes       
Client on kn4-example.com:  [  5]   8.00-9.00   sec  98.7 MBytes   828 Mbits/sec   23    246 KBytes       
Client on kn4-example.com:  [  5]   9.00-10.00  sec  96.8 MBytes   812 Mbits/sec    0    443 KBytes       
Client on kn4-example.com:  - - - - - - - - - - - - - - - - - - - - - - - - -
Client on kn4-example.com:  [ ID] Interval           Transfer     Bitrate         Retr
Client on kn4-example.com:  [  5]   0.00-10.00  sec  1.01 GBytes   864 Mbits/sec  171             sender
Client on kn4-example.com:  [  5]   0.00-10.00  sec  1.00 GBytes   863 Mbits/sec                  receiver
Client on kn4-example.com:  
Client on kn4-example.com:  iperf Done.
####
[Cleanup]
deployment.apps "kube-net-diag-deployment" deleted
service "kube-net-diag" deleted
daemonset.apps "kube-net-diag-clients" deleted
####
#### The measurements took 240 seconds to execute!
####
```

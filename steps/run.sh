#!/usr/bin/env bash
set -eu

CLIENTS=$(kubectl --request-timeout 60 get pods -l app=kube-net-diag-client -o name | cut -d'/' -f2 2>&1)

for POD in ${CLIENTS}; do
    until $(kubectl --request-timeout 60 get pod ${POD} -o jsonpath='{.status.containerStatuses[0].ready}' 2>&1); do
        echo "#### Waiting for ${POD} to start"
        sleep 15
        echo "####"
    done
    HOST=$(kubectl --request-timeout 60 get pod ${POD} -o jsonpath='{.spec.nodeName}' 2>&1 || :)
    echo "####"
    echo "#### Mtr on ${HOST} connecting to pod on target node $@"
    kubectl --request-timeout 60 exec -it ${POD} -- mtr -brnTwc 30 kube-net-diag-server $@ 2>&1 || :
    echo "####"
    echo "####"
    kubectl --request-timeout 60 exec -it ${POD} -- iperf3 -c kube-net-diag-server -T "Client on ${HOST}" $@ 2>&1 || :
    echo "####"
done

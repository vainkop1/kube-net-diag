#!/usr/bin/env bash
set -eu

echo "####"
echo "#### Usage: kube-net-diag.sh <Server Node>"
echo "####"


SERVER_NODE=$1; shift

if [[ ! $(kubectl --request-timeout 60 get node -o name | grep "node/${SERVER_NODE}" 2>&1) ]]; then
    >&2 echo "#### [Error] Node ${SERVER_NODE} not found!"
    print_usage
    exit 1
fi

echo "####"
echo "#### Checking possible previous installations"
echo "####"
if [[ ! $(kubectl --request-timeout 60 get pods -l app=iperf3-server 2> /dev/null && kubectl --request-timeout 60 get svc -l app=kube-net-diag-server 2> /dev/null && kubectl --request-timeout 60 get ds -l app=kube-net-diag-client 2> /dev/null) ]]; then
    cat kube-net-diag.yaml | sed -s "s/{NODE}/${SERVER_NODE}/g" | kubectl --request-timeout 60 apply -f - 2>&1
elif [[ $(kubectl --request-timeout 60 get pod -l app=kube-net-diag-server -o jsonpath='{.items[0].metadata.labels.node}' 2>&1) != "${SERVER_NODE}" ]]; then
    >&2 echo "#### Previous installation detected!"
    >&2 echo "$(kubectl --request-timeout 60 get pods -l app=kube-net-diag-server -o wide 2>&1)"
    >&2 echo "####"
    >&2 echo "#### Deleting previous installation"
    >&2 echo "$(kubectl --request-timeout 60 delete -f kube-net-diag.yaml 2>&1 || :)"
    echo "####"
fi

echo "####"
echo "#### Starting kube-net-diag-server"
echo "####"
kubectl --request-timeout 60 apply -f kube-net-diag.yaml 2>&1

until $(kubectl --request-timeout 60 get pods -l app=kube-net-diag-server -o jsonpath='{.items[0].status.containerStatuses[0].ready}' 2>&1); do
    echo "####"
    echo "#### Waiting for kube-net-diag-server to start on node ${SERVER_NODE}"
    sleep 5
done

echo "####"
echo "#### kube-net-diag-server is running on node ${SERVER_NODE} !"
echo "####"

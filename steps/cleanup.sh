#!/usr/bin/env bash
set -eu

kubectl --request-timeout 60 delete --cascade -f kube-net-diag.yaml 2>&1 || :

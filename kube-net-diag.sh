#!/usr/bin/env bash
set -eu

start=$(date +%s)

cd $(dirname $0)

echo "[Setup]"
steps/setup.sh $@

echo "[Run]"
steps/run.sh $@

echo "[Cleanup]"
steps/cleanup.sh

end=$(date +%s)

echo "####"
echo "#### The measurements took $((end-start)) seconds to execute!"
echo "####"

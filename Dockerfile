FROM ubuntu:20.04

MAINTAINER Valerii Vainkop

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y iperf3 mtr && \
    rm -rf /var/lib/apt/lists/*

EXPOSE 5201/tcp 5201/udp

ENTRYPOINT ["iperf3"]
CMD ["-s"]
